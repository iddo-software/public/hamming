# Hamming Distance
A sample [`C/C++`](https://isocpp.org/) library that computes the [Hamming distance](https://en.wikipedia.org/wiki/Hamming_distance) between two byte strings.

I've written the code for this library in late 2016, as part of the interview process with the good people at a recruitment agency.

The code was designed to showcase the skill of writing production-level C++ code. It features:
- cross-platform, cross-compiler and cross-IDE build support:
    - [`Cmake`](https://cmake.org/)
    - compiler awareness in the code:
        - [`gcc`](https://gcc.gnu.org/) on Linux and Windows ([mingw](http://www.mingw.org/) / [Cygwin](https://www.cygwin.com/))
        - [MSVC](https://visualstudio.microsoft.com/vs/features/cplusplus/) compiler on windows
        - [`icc`](https://software.intel.com/content/www/us/en/develop/tools/compilers/c-compilers.html) on Linux and Windows for extra performance
- high-level `C++` API
- low-level `C` API for cross-compiler ABI compatibility at library load time (including specifying calling conventions)
- support for keeping consistency of compiler flags when building against the library using [`Cmake`](https://cmake.org/)'s `install` features 
- unit tests (using [GTest](https://github.com/google/googletest))
- sample executable that feeds the library with data from files
- multiple implementations to benchmark before deploying on a specific platform
    - use specialized CPU instructions where the platform implements a specific ISA (implmeneted with compiler intrinsics specific to each supported compiler)
- thread-level parallelism using [OpenMP](https://www.openmp.org/)
- tries to showcase a sound and consistent API design and good overall coding practices, like:
    - exception handling for the `C++` high-level API that works across dll boundaries
    - use of modern `C++` standards where applicable, useful / necessary, such as heavy reliance on `stl` for RAII memory management etc.
    - encapsulation, code reuse (e.g., using templates)

The library does NOT have the following features, although it would have made a fair point for our skills (and fun to write). I decided to leave these out in favor of the above in the context of a limited time budget.
- documentation (the plan was to do some doxygen)
- automated benchmarking of the available alternative implementations at installation time

Now, at the time of this writing (end of 2020, i.e., almost 4 years after writing the code), my `C/C++` skills are a bit rusty, and I'm a bit out of date with the newest standards and practices. Back when I wrote the code I was on top of my `C/C++` game, right after 6 years of coding `C/C++` almost every day, and following the then-latest language advancements. Since then, I've switched entirely to [`Python`](https://www.python.org).

In the mean time, though (summer of 2019?), I had to write some `C/C++` code for a client, and was surprised to remember more about the language than I thought I would, and managed to deliver a working (and I would also say pretty) solution.

# Installation
Build using cmake. The easiest way is to use the gui, where you can select the
algorithms that you want to include in the build, and the location to which
you want your project files saved. 

Be sure to select a proper `CMAKE_INSTALL_PREFIX` (especially on
windows; on linux the default is `/usr/local/` - you probably don't want to
install there, either, as this is just a test library you'll probably forget
about).

If you want the production version (i.e. stripped symbols), you should turn
`HAMMING_BUILD_TESTS OFF`.

You can also select the build type: `Release` or `Debug` (or both, if you're using
`MSVC`).

Ok, now configure the thing, and generate the project files for your favorite
IDE. On Windows that's probably Visual Studio, on Linux, makefiles. If you're
on linux, go to the build directory and run make install. If you're using MSVC,
open the solution in the build folder, and build the project named `CMAKE_INSTALL`.
This step should make sure that all the binaries (and lib's includes) are in 
`CMAKE_INSTALL_PREFIX`.

For using the `exe`, go to `CMAKE_INSTALL_PREFIX/bin` and execute it. Note that you
need to add `<install path>/lib` to the executable's loader paths before being 
able to run (you can do this via `LD_CONFIG_PATH` environment variable on linux, or by specifying 
the environemnt variables in the debug options of your executable project in 
MSVC) on linux you could do something along the lines of
```bash
hamming_install/bin$ LD_LIBRARY_PATH=../lib ./hamming
```

To execute the tests, run the hamming_test executable in `<install path>/bin`,
the same way you would run the executable.
 
If you want another executable (client module) to link to the library, the
easiest way to pass the relevant precompiler definitions is by using cmake. 

In the client's `CMakeLists.txt`, you just need to write

```cmake
find_package(hamming)
add_executable(<my_executable_name> ...)
target_link_libraries(<my_executable_name> hamming)
```

and set the hamming path to the install path that you specified when building.

That's it.
